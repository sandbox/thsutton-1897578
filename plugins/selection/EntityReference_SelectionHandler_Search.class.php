<?php
/**
 * @file
 *
 * Implement entity reference autocompletion functionality using Search API
 * indexes.
 */

/**
 * Entity handler for Views.
 */
class EntityReference_SelectionHandler_Search implements EntityReference_SelectionHandler {

  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {

    $target_entity_type = $field['settings']['target_type'];
    $entity_info = entity_get_info($target_entity_type);

    # TODO: Check that there's an index for this entity type.
    if (FALSE) {
      return EntityReference_SelectionHandler_Broken::getInstance($field, $instance);
    }

    return new EntityReference_SelectionHandler_Search($field, $instance);
  }

  /**
   * Constructor.
   */
  protected function __construct($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    $this->field = $field;
    $this->instance = $instance;
    $this->entity_type = $entity_type;
    $this->entity = $entity;
  }

  /**
   * Get all configured indexes which are compatible with the field.
   *
   * @see search_api_index_load_multiple()
   *
   * @return array
   *   An array of index objects keyed by machine name.
   */
  protected static function availableIndexes($field, $instance) {
    $indexes = array();

    $entity_info = entity_get_info($field['settings']['target_type']);

    foreach (search_api_index_load_multiple(FALSE) as $index) {
      // @todo Check that the index is compatible with the field.
      if (TRUE) {
        $indexes[$index->machine_name] = $index;
      }
    }
    return $indexes;
  }

  /**
   * Implements EntityReferenceHandler::settingsForm().
   */
  public static function settingsForm($field, $instance) {
    $form = array();
    $entity_info = entity_get_info($field['settings']['target_type']);

    // Merge-in default values.
    $field['settings']['handler_settings'] += array(
      'target_bundles' => array(),
      'sort' => array(
        'type' => 'search__score',
        'settings' => array(
          'field' => NULL,
        ),
      )
    );

    // Find a list of all Search API indexes which contain the entity type.
    $indexes = self::availableIndexes($field, $instance);

    $form['index'] = array(
      '#type' => 'select',
      '#title' => t('Search index'),
      '#description' => t('Select the search index to be used for autocompletion.'),
      '#options' => array(),
      '#default_value' => $field['settings']['handler_settings']['index'],
      '#required' => TRUE,
      '#ajax' => TRUE,
      '#limit_validation_errors' => array(),
    );

    foreach ($indexes as $machine_name => $index) {
      $form['index']['#options'][$machine_name] = $index->name;
    }

    // If an index has been selected, include other form fields.
    if (!empty($field['settings']['handler_settings']['index'])) {
      $searchindex = $indexes[$field['settings']['handler_settings']['index']];

      # @TODO Include options from SearchApiQueryInterface::parseModes().

      // Handle sorting
      $form['sort']['type'] = array(
        '#type' => 'select',
        '#title' => t('Sort by'),
        '#options' => array(
          'none' => t("Don't sort"),
          'score' => t("Search ranking"),
          'field' => t('A field included in this index'),
        ),
        '#ajax' => TRUE,
        '#limit_validation_errors' => array(),
        '#default_value' => $field['settings']['handler_settings']['sort']['type'],
      );

      $form['sort']['settings'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('entityreference-settings')),
        '#process' => array('_entityreference_form_process_merge_parent'),
      );

      if ($field['settings']['handler_settings']['sort']['type'] == 'field') {
        $fields = array();
        foreach ($searchindex->getFields() as $key => $indexfield) {
          # @TODO Only include fields which are sortable.
          if (TRUE) {
            $fields[$key] = t('Field @name (@key)', array(
              '@key' => $key,
              '@type' => $indexfield['type'],
              '@name' => $indexfield['name'],
              '@description' => $indexfield['description'],
              '@boost' => $indexfield['boost'],
              '@indexed' => $indexfield['indexed'],
            ));
          }
        }

        $form['sort']['settings']['field'] = array(
          '#type' => 'select',
          '#title' => t('Sort field'),
          '#description' => t('Select the field which should be used to sort the selected entities.'),
          '#required' => TRUE,
          '#options' => $fields,
          '#default_value' => $field['settings']['handler_settings']['sort']['settings']['field'],
        );

      }
    }

    return $form;
  }

  /**
   * Implements EntityReferenceHandler::getReferencableEntities().
   */
  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = array();

    $entity_type = $this->field['settings']['target_type'];
    $index = $this->field['settings']['handler_settings']['index'];

    // Get the Search API index and start a query.
    $this->search_index = search_api_index_load($index);
    //watchdog("ersaa", "Here's the thing we got: <pre>" . print_r($this->search_index, TRUE) . '</pre>') ;
    $this->search_query = $this->search_index->query(array(
      'parse mode' => 'terms',
    ));

    // Filter for content type, etc.
    //$this->search_query->condition('entity_type', $entity_type, '=');

    // Add search terms.
    $this->search_query->keys($match);

    $sort = $this->field['settings']['handler_settings']['sort']['type'];
    switch ($sort) {
      case 'score':
        $this->search_query->sort('search_api_relevance');
        break;

      case 'field':
        $sort_field = $this->field['settings']['handler_settings']['sort']['settings']['field'];
        $this->search_query->sort($sort_field);
        break;
    }

    // Limit the number of results.
    if ($limit) {
      $this->search_query->range(0, $limit);
    }

    // Run the query.
    $hits = $this->search_query->execute();

    watchdog('ersaa', "Found the following results: <pre>" . print_r($hits, TRUE) . "</pre>");

    if (!empty($hits['results'])) {
      $entities = entity_load($entity_type, array_keys($hits['results']));
      foreach ($entities as $entity_id => $entity) {
        list(,, $bundle) = entity_extract_ids($entity_type, $entity);
        $options[$bundle][$entity_id] = check_plain($this->getLabel($entity));
      }
    }

    return $options;
  }

  /**
   * Implements EntityReferenceHandler::countReferencableEntities().
   */
  public function countReferencableEntities($match = NULL, $match_operator = 'CONTAINS') {
    $matches = $this->getReferencableEntities($match, $match_operator);

    $type = $this->field['settings']['target_type'];

    return count($matches[$type]);
  }

  /**
   * Implements EntityReferenceHandler::validateReferencableEntities().
   */
  public function validateReferencableEntities(array $ids) {
    # TODO: Filter $ids to be only those entities present in current search results.
    return $ids;
  }

  /**
   * Implements EntityReferenceHandler::validateAutocompleteInput().
   */
  public function validateAutocompleteInput($input, &$element, &$form_state, $form) {
      $entities = $this->getReferencableEntities($input, '=', 6);
      if (empty($entities)) {
        // Error if there are no entities available for a required field.
        form_error($element, t('There are no entities matching "%value"', array('%value' => $input)));
      }
      elseif (count($entities) > 5) {
        // Error if there are more than 5 matching entities.
        form_error($element, t('Many entities are called %value. Add more keywords to narrow your search.', array(
          '%value' => $input,
        )));
      }
      elseif (count($entities) > 1) {
        // More helpful error if there are only a few matching entities.
        $multiples = array();
        foreach ($entities as $id => $name) {
          $multiples[] = $name . ' (' . $id . ')';
        }
        form_error($element, t('Multiple entities match this reference; "%multiple"', array('%multiple' => implode('", "', $multiples))));
      }
      else {
        // Take the one and only matching entity.
        return key($entities);
      }
  }

  /**
   * Implements EntityReferenceHandler::entityFieldQueryAlter().
   */
  public function entityFieldQueryAlter(SelectQueryInterface $query) {

  }

  /**
   * Implements EntityReferenceHandler::getLabel().
   */
  public function getLabel($entity) {
    return entity_label($this->field['settings']['target_type'], $entity);
  }

}
