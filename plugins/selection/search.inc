<?php
/**
 * @file
 * Define the 'search' plugin.
 */

if (module_exists('search_api')) {
  $plugin = array(
    'title' => t('Search: Filter using a Search API index'),
    'class' => 'EntityReference_SelectionHandler_Search',
    'weight' => 0,
  );
}
