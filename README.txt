Entity Reference Autocompletion with Search 
===========================================

This module implements an selection plugin for the Entity Reference module's
autocompletion widgets based on Search API indexes.

Unlike the default selection plugin, this module allows users to find the
entity to be referenced using full-text searching backed by Search API. This is
probably pointless, but it was fun to hack on.

Security
--------

No attempt has been made to ensure that this module is secure. In particular,
no effort has been taken to ensure that the user has access to the suggestions
offered by the plugin.

Also: it's probably full of XSS.

Configuration
-------------

1. Install [Entity Reference][1], [Search API][2] and [this module][3] (along
   with Field UI).

2. Create a Search API index including the entities you want to reference.

3. Add an *Entity Reference* field using the *Autocomplete* widget to a
   fieldable entity.

4. Configure the new field to use the correct target type (the entities in your
   search index) and select the *Search: Filter using a Search API index* mode.

   The available search indexes will be listed, choose the correct index.
   Configure sorting and other parameters as you prefer.

5. Save the field and go and try it out!

[1]: http://drupal.org/project/entityreference
[2]: http://drupal.org/project/search_api
[3]: http://drupal.org/sandbox/thsutton/1897578
